<?php
namespace common\models;

use nemmo\attachments\behaviors\FileBehavior;
use nemmo\attachments\ModuleTrait;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use nemmo\attachments\models\File;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $mobile
 */
class User extends ActiveRecord implements IdentityInterface
{
    use ModuleTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'fileBehavior' => [
                'class' => FileBehavior::className()
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status',  'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['email', 'email'],
            ['mobile', 'udokmeci\yii2PhoneValidator\PhoneValidator','country'=>'DE'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /*
     * Deactivates user
     */
    public function deactivate()
    {
        $this->status = self::STATUS_DELETED;
        $this->save();
    }

    /**
     * Returns url of user avatar
     *
     * @return null|string
     */
    public function getAvatarUrl()
    {
        /* @var File $file */
        foreach ($this->files as $file) {
            /*
             * FIXME: Use a UrlManager here to get the frontend base url
             */
            return "http://frontend/" . $file->getUrl();
        }
        return null;
    }

    /*
     * Returns an array of user statuses
     *
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_DELETED
        ];
    }

    /*
     * Returns text for given status or model status
     */
    public function getStatusText($status = null)
    {
        $status = $status === null ? $this->status : $status;

        switch($status) {
            case self::STATUS_DELETED:
                return "Deleted";
            case self::STATUS_ACTIVE:
                return "Active";
        }
        return null;
    }

    /*
     * Returns associative array of statuses for dropdown
     */
    public function getStatusMapping()
    {
        $result = [];
        foreach($this->getStatuses() as $status) {
            $result[$status] = $this->getStatusText($status);
        }
        return $result;
    }

    /*
     * Ensures that old avatar uploads are being deleted
     */
    public function cleanUpAttachments()
    {
        // loop through all files but the latest and delete
        /* @var File $file */
        for($i=count($this->files)-2; $i>=0; $i--) {
            $file = $this->files[$i];
            $this->getModule()->detachFile($file->id);
        }
    }

    /*
     * Extends module attributes
     */
    public function __get($name)
    {
        switch ($name) {
            case "avatar":
                return $this->getAvatarUrl();
            case "statusText":
                return $this->getStatusText();
        }
        return parent::__get($name);
    }
}
