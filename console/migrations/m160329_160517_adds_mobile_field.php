<?php

use yii\db\Migration;

class m160329_160517_adds_mobile_field extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'mobile', $this->string());
    }

    public function down()
    {
        $this->dropColumn('user', 'mobile');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}


