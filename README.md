# Flexperto FAT

## Tools used:
- Scotchbox (Vagrant LAMP)
- Yii2 with Gii
- XDebug
- PhpStorm
- Sequel Pro
- Google Chrome
- git

## Composer packages that were included
- nemmo/yii2-attachments
- udokmeci/yii2-phone-validator

## Setting up the environment
Follow these instructions to get the exact environment, that has been used for development.

```
vagrant up
vagrant ssh
	sudo su
		more /var/www/vhosts.conf > /etc/apache2/sites-available/000-default.conf
		exit
	sudo service apache2 restart
	composer global require "fxp/composer-asset-plugin:*"
	composer install
	#follow instruction for github token
	php init #choose Development environment
	mysql -uroot -proot
		create database fat;
		exit
	ifconfig #copy ip from vagrant box (probably 192.168.33.10) to clipboard
```
## Host Configuration
Now paste the IP into the host configuration of your machine for front- and backend like this:
```
192.168.33.10 frontend backend
```

## Database
Edit the file common/config/main-local.php and 
- replace “dbname=yii2advanced” with “dbname=fat”
- add the password “root”

## Migrations
```
vagrant ssh
	cd /var/www/
	php yii migrate/up --migrationPath=@vendor/nemmo/yii2-attachments/migrations
	php yii migrate
```